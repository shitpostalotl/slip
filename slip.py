# nix-shell --pure -p python3 git [image thing (i use feh)]
import sys, os

file = open(sys.argv[1],"r")
slides = file.read().split("---\n")
file.close()

print("\x1b[?25l")
for i in range(len(slides)):
	if slides[i][0] == ">":
		l = slides[i].split("\n")
		l[0] = "\u001b[4m"+l[0][1:]+"\u001b[0m"
		slides[i] = "\n".join(l)
	elif slides[i][0] == "$":
		try: os.startfile(slides[i][0][1:])
		except AttributeError: os.system(sys.argv[2]+" image.png")

	rows, cols = os.popen("stty size", "r").read().split()
	gotobottom = "\n"*(int(rows)-slides[i].count("\n")-1)
	progress = str(i+1)+"/"+str(len(slides))
	progbar = int(((i+1)/len(slides))*int(cols)-len(progress))*"█"

	print(f"\u001b[2J \u001b[0;0H{slides[i][:-1]}{gotobottom}\u001b[46m\u001b[30m{progress}\033[036m{progbar}\033[0m")
	input("\u001b[1F")

print("\u001b[2J \u001b[0;0H\x1b[?25h")
